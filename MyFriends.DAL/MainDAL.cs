﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyFriends.EL;

namespace MyFriends.DAL
{
    public class MainDAL
    {
        public bool AddAsFriend(string UserId, string FriendId)
        {
            using (MyFriendsEntities db = new MyFriendsEntities())
            {
                db.Friendship.Add(new Friendship { UserId = UserId, FriendsId = FriendId });
                db.SaveChanges();

                db.Friendship.Add(new Friendship { UserId = FriendId, FriendsId = UserId});
                db.SaveChanges();
            }
            return true;
        }

        public List<User> AllUsers(string UserId)
        {
            List<User> Users = new List<User>();
            using (MyFriendsEntities db = new MyFriendsEntities())
            {
                Users = (from u in db.AspNetUsers
                            from fp in db.Friendship.Where(x => x.UserId == u.Id && x.FriendsId == UserId).DefaultIfEmpty()
                            select new User
                            {
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                Email = u.Email,
                                UserId = u.Id,
                                DateOfBirth = u.DateOfBirth,
                                FriendId = fp.FriendsId
                            }).ToList();
                    
                // Map to users object
                //Users.AddRange(AspUsers.Select(x=> new User { UserId = x.Id, FirstName = x.FirstName, LastName = x.LastName, DateOfBirth = x.DateOfBirth, Email = x.Email}));
            }
            return Users;
        }

        public List<User> MyFriendsList(string UserIdP)
        {
            List<User> Users = new List<User>();
            using (MyFriendsEntities db = new MyFriendsEntities())
            {
                Users = (
                        from fp in db.Friendship
                        from u in db.AspNetUsers.Where(x => x.Id == fp.FriendsId)
                         where fp.UserId == UserIdP
                        select new User
                         {
                             FirstName = u.FirstName,
                             LastName = u.LastName,
                             Email = u.Email,
                             UserId = u.Id,
                             DateOfBirth = u.DateOfBirth,
                             FriendId = fp.FriendsId
                         }).ToList();
            }
            return Users;
        }
    }
}
