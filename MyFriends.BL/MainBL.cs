﻿using MyFriends.DAL;
using MyFriends.EL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFriends.BL
{
    public class MainBL
    {
        private MainDAL mainDal;
        public MainBL() {
            mainDal = new MainDAL();
        }

        public List<User> AllUsers(string UserId) {
            return mainDal.AllUsers(UserId);
        }

        public bool AddAsFriend(string UserId, string FriendId)
        {
            return mainDal.AddAsFriend(UserId, FriendId);
        }
        public List<User> MyFriendsList(string UserId)
        {
            return mainDal.MyFriendsList(UserId);
        }
    }
}
