﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyFriends.Startup))]
namespace MyFriends
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
