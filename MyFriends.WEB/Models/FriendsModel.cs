﻿using MyFriends.EL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFriends.BL;

namespace MyFriends.WEB.Models
{
    public class FriendsModel{
        
        public bool AddAsFriend(string UserId, string FriendId)
        {
            MainBL blmain = new MainBL();
            return blmain.AddAsFriend(UserId, FriendId);
        }
    }
}
