﻿using MyFriends.EL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFriends.BL;

namespace MyFriends.WEB.Models
{
    public class UserModel:User
    {
        public List<User> UsersList { get; set; }
        public List<User> FriendsList { get; set; }

        public List<User> GetAllUsers(string UserId)
        {
            MainBL blmain = new MainBL();
            return blmain.AllUsers(UserId);
        }
        public List<User> MyFriendsList(string UserId)
        {
            MainBL blmain = new MainBL();
            return blmain.MyFriendsList(UserId);
        }
    }
}
