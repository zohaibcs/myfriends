﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyFriends.WEB.Startup))]
namespace MyFriends.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
