﻿using MyFriends.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MyFriends.WEB.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            UserModel model = new UserModel();
            model.FriendsList = model.MyFriendsList(User.Identity.GetUserId());
          
            return View(model);
        }

        [Authorize]
        public ActionResult AllUsers()
        {
            UserModel model = new UserModel();
            ViewBag.UserId = User.Identity.GetUserId();
            model.UsersList = model.GetAllUsers(ViewBag.UserId);
            return View(model); 
        }

        [Authorize]
        public ActionResult AddAsFriend(string FriendId)
        {
            FriendsModel model = new FriendsModel();
            string UserId = User.Identity.GetUserId();
            model.AddAsFriend(UserId, FriendId);
            return Redirect("AllUsers");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}